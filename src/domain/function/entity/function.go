package entity

import (
	"fmt"
	"github.com/cloudwego/kitex/pkg/klog"
	"gitlab.com/bishe-projects/common_utils/business_error"
	"gitlab.com/bishe-projects/function_service/src/infrastructure/function_error"
	"gitlab.com/bishe-projects/function_service/src/infrastructure/repo"
	"gitlab.com/bishe-projects/function_service/src/infrastructure/repo/po"
)

type Function struct {
	ID   int64
	Name string
	Desc string
	Path string
	Icon string
}

func (f *Function) GetFunctionByID(functionID int64) *business_error.BusinessError {
	functionPO, err := repo.FunctionMySQLRepo.FunctionDao.GetFunctionByID(functionID)
	if err != nil {
		klog.Errorf("[FunctionAggregate] get function by id failed: err=%s", err)
		return function_error.GetFunctionByIDErr
	}
	f.fillFunctionFromFunctionPO(functionPO)
	return nil
}

type FunctionList []*Function

func (fl *FunctionList) AllFunctionList() *business_error.BusinessError {
	functionPOList, err := repo.FunctionMySQLRepo.FunctionDao.AllFunctionList()
	if err != nil {
		klog.Errorf("[FunctionAggregate] get all function list failed: err=%s", err)
		return function_error.GetAllFunctionListErr
	}
	fl.fillFunctionListFromFunctionPOList(functionPOList)
	return nil
}

// converter
func (f *Function) fillFunctionFromFunctionPO(functionPO *po.Function) {
	f.ID = functionPO.ID
	f.Name = functionPO.Name
	f.Desc = functionPO.Desc
	f.Path = functionPO.Path
	f.Icon = functionPO.Icon
}

func (fl *FunctionList) fillFunctionListFromFunctionPOList(functionPOList []*po.Function) {
	*fl = make([]*Function, 0, len(functionPOList))
	for _, functionPO := range functionPOList {
		function := new(Function)
		function.fillFunctionFromFunctionPO(functionPO)
		*fl = append(*fl, function)
	}
}

func (f *Function) String() string {
	return fmt.Sprintf("{id=%d, name=%s, desc=%s, path=%s, icon=%s}", f.ID, f.Name, f.Desc, f.Path, f.Icon)
}
