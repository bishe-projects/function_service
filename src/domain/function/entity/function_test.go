package entity

import (
	"gitlab.com/bishe-projects/common_utils/business_error"
	"gitlab.com/bishe-projects/function_service/src/infrastructure/function_error"
	"reflect"
	"testing"
)

func TestFunction_GetFunctionByID(t *testing.T) {
	type fields struct {
		ID   int64
		Name string
		Desc string
	}
	type args struct {
		functionID int64
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   *business_error.BusinessError
	}{
		{name: "found", fields: fields{}, args: args{1}, want: nil},
		{name: "not found", fields: fields{}, args: args{-1}, want: function_error.GetFunctionByIDErr},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &Function{
				ID:   tt.fields.ID,
				Name: tt.fields.Name,
				Desc: tt.fields.Desc,
			}
			if got := d.GetFunctionByID(tt.args.functionID); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetFunctionByID() = %v, want %v", got, tt.want)
			}
			t.Logf("Function=%+v", d)
		})
	}
}
