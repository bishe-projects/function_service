package service

import (
	"gitlab.com/bishe-projects/common_utils/business_error"
	"gitlab.com/bishe-projects/function_service/src/domain/function/entity"
)

var FunctionDomain = new(Function)

type Function struct{}

func (d *Function) GetFunctionByID(functionID int64) (*entity.Function, *business_error.BusinessError) {
	function := new(entity.Function)
	err := function.GetFunctionByID(functionID)
	return function, err
}

func (d *Function) AllFunctionList() ([]*entity.Function, *business_error.BusinessError) {
	functionList := new(entity.FunctionList)
	err := functionList.AllFunctionList()
	return *functionList, err
}