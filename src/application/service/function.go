package service

import (
	"gitlab.com/bishe-projects/common_utils/business_error"
	"gitlab.com/bishe-projects/function_service/src/domain/function/entity"
	"gitlab.com/bishe-projects/function_service/src/domain/function/service"
)

var FunctionApp = new(Function)

type Function struct{}

func (a *Function) GetFunctionByID(functionID int64) (*entity.Function, *business_error.BusinessError) {
	return service.FunctionDomain.GetFunctionByID(functionID)
}

func (a *Function) AllFunctionList() ([]*entity.Function, *business_error.BusinessError) {
	return service.FunctionDomain.AllFunctionList()
}