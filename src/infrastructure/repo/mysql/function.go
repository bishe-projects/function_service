package mysql

import (
	"gitlab.com/bishe-projects/function_service/src/infrastructure/repo/po"
)

var (
	FunctionMySQLDao = new(Function)
)

type Function struct{}

const FunctionTable = "function"

func (r *Function) GetFunctionByID(functionID int64) (*po.Function, error) {
	var function *po.Function
	result := db.Table(FunctionTable).First(&function, functionID)
	return function, result.Error
}

func (r *Function) AllFunctionList() ([]*po.Function, error) {
	var functionList []*po.Function
	result := db.Table(FunctionTable).Find(&functionList)
	return functionList, result.Error
}