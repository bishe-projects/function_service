package mysql

import (
	"testing"
)

func TestFunction_GetFunctionByID(t *testing.T) {
	type args struct {
		functionID int64
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{name: "found", args: args{1}, wantErr: false},
		{name: "not found", args: args{-1}, wantErr: true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Function{}
			_, err := r.GetFunctionByID(tt.args.functionID)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetFunctionByID() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
		})
	}
}
