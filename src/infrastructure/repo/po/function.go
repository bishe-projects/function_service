package po

import "fmt"

type Function struct {
	ID   int64
	Name string
	Desc string
	Path string
	Icon string
}

func (po *Function) String() string {
	return fmt.Sprintf("{id=%d, name=%s, desc=%s, path=%s, icon=%s}", po.ID, po.Name, po.Desc, po.Path, po.Icon)
}
