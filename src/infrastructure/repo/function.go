package repo

import (
	"gitlab.com/bishe-projects/function_service/src/infrastructure/repo/mysql"
	"gitlab.com/bishe-projects/function_service/src/infrastructure/repo/po"
)

type FunctionDao interface {
	GetFunctionByID(int64) (*po.Function, error)
	AllFunctionList() ([]*po.Function, error)
}

var FunctionMySQLRepo = NewFunctionRepo(mysql.FunctionMySQLDao)

type Function struct {
	FunctionDao FunctionDao
}

func NewFunctionRepo(functionDao FunctionDao) *Function {
	return &Function{
		FunctionDao: functionDao,
	}
}