package function_error

import "gitlab.com/bishe-projects/common_utils/business_error"

// internal server error
var (
	GetFunctionByIDErr = business_error.NewBusinessError("get function by id failed", -10001)
	FunctionNotExistsErr = business_error.NewBusinessError("function not exists", -10002)
	GetAllFunctionListErr = business_error.NewBusinessError("get all function list failed", -10003)
)
