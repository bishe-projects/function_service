package facade

import (
	"context"
	"github.com/cloudwego/kitex/pkg/klog"
	"gitlab.com/bishe-projects/common_utils/business_error"
	"gitlab.com/bishe-projects/function_service/src/application/service"
	"gitlab.com/bishe-projects/function_service/src/interface/assembler"
	"gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/function"
)

var FunctionFacade = new(Function)

type Function struct {}

func (f *Function) GetFunctionByID(ctx context.Context, req *function.GetFunctionByIDReq) *function.GetFunctionByIDResp {
	resp := &function.GetFunctionByIDResp{}
	functionEntity, err := service.FunctionApp.GetFunctionByID(req.FunctionId)
	if err != nil {
		klog.CtxErrorf(ctx, "[FunctionFacade] get function by id failed: req=%+v err=%s", req, err)
		resp.BaseResp = business_error.BaseResp(err)
		return resp
	}
	resp.Function = assembler.ConvertFunctionEntityToFunction(functionEntity)
	return resp
}

func (f *Function) AllFunctionList(ctx context.Context, req *function.AllFunctionListReq) *function.AllFunctionListResp {
	resp := &function.AllFunctionListResp{}
	functionList, err := service.FunctionApp.AllFunctionList()
	if err != nil {
		klog.CtxErrorf(ctx, "[FunctionFacade] get all function list failed: req=%+v err=%s", req, err)
		resp.BaseResp = business_error.BaseResp(err)
		return resp
	}
	resp.FunctionList = assembler.ConvertFunctionEntityListToFunctionList(functionList)
	return resp
}