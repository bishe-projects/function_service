package assembler

import (
	"gitlab.com/bishe-projects/function_service/src/domain/function/entity"
	"gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/function"
)

func ConvertFunctionEntityToFunction(functionEntity *entity.Function) *function.Function {
	return &function.Function{
		Id:   functionEntity.ID,
		Name: functionEntity.Name,
		Desc: functionEntity.Desc,
		Path: functionEntity.Path,
		Icon: functionEntity.Icon,
	}
}

func ConvertFunctionEntityListToFunctionList(functionEntityList []*entity.Function) []*function.Function {
	functionList := make([]*function.Function, 0, len(functionEntityList))
	for _, functionEntity := range functionEntityList {
		functionList = append(functionList, ConvertFunctionEntityToFunction(functionEntity))
	}
	return functionList
}
