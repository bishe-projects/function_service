package main

import (
	"context"
	"github.com/cloudwego/kitex/pkg/klog"
	"gitlab.com/bishe-projects/function_service/src/interface/facade"
	"gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/function"
)

// FunctionServiceImpl implements the last service interface defined in the IDL.
type FunctionServiceImpl struct{}

// GetFunctionByID implements the FunctionServiceImpl interface.
func (s *FunctionServiceImpl) GetFunctionByID(ctx context.Context, req *function.GetFunctionByIDReq) (resp *function.GetFunctionByIDResp, err error) {
	klog.CtxInfof(ctx, "GetFunctionByID req=%+v", req)
	resp = facade.FunctionFacade.GetFunctionByID(ctx, req)
	klog.CtxInfof(ctx, "GetFunctionByID resp=%+v", resp)
	return
}

// AllFunctionList implements the FunctionServiceImpl interface.
func (s *FunctionServiceImpl) AllFunctionList(ctx context.Context, req *function.AllFunctionListReq) (resp *function.AllFunctionListResp, err error) {
	klog.CtxInfof(ctx, "AllFunctionList req=%+v", req)
	resp = facade.FunctionFacade.AllFunctionList(ctx, req)
	klog.CtxInfof(ctx, "AllFunctionList resp=%+v", resp)
	return
}
