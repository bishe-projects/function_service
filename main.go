package main

import (
	"github.com/cloudwego/kitex/server"
	function "gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/function/functionservice"
	"log"
	"net"
)

func main() {
	addr, _ := net.ResolveTCPAddr("tcp", "127.0.0.1:8880")
	svr := function.NewServer(new(FunctionServiceImpl), server.WithServiceAddr(addr))

	err := svr.Run()

	if err != nil {
		log.Println(err.Error())
	}
}
